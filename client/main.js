'use strict';

var http = require('http');
var fs = require('fs');

var path = 'http://localhost:8888/gui/';

var magnet = '6ffbf96c48920c986a07bf281f6e0f08b92c781a';
var filePath = '/home/maxim/Downloads/TriblerDownloads/test33';
var key = '123489732487328947123947';

var testdir = '/home/maxim/Downloads/triblertest';
var torrtestdir = '/home/maxim/Downloads/torrtest';
var aggrName = 'aggr.txt';

function main () {
	var request = http.get(path + 'token-json', function (response) {
		response.on('data', function (data) {
			var response = data.toString();
			var token = JSON.parse(response).token;
			//checkTorrents(token);
			//test(token);
			//iterateUpload(token);
			iterateAdding(token);
			//iterateUploadAggr(token);
			//iterateAddingAggr(token);
			//getConnected(token);
			//getFile(magnet, token, key);
			//getProgress(magnet, token);
			//addMagnet(magnet, token);
			//uploadFile(filePath, token);
			//quit(token);
		});
	});
	request.on('error', function (error) {
		console.log(error);
	});
}

function addMagnet(magnet, token, callback) {
	var url = path + '?token=' + token + '&action=add-url&s=' + magnet;
	console.log(url);
	var request = http.get(url, function (response) {
		response.on('data', function (data) {
			console.log(data.toString());
			if (callback) {
				callback();
			}
		});
	})
}

function addTorrent(filePath, token, callback) {
	var url = path + '?token=' + token + '&action=add-torrent&path=' + encodeURIComponent(filePath);
	console.log(url);
	var request = http.get(url, function (response) {
		response.on('data', function (data) {
			console.log(data.toString());
			if (callback) {
				callback();
			}
		});
	});
	request.on('error', function(err) {
		console.log(err);
	});
}

function uploadFile(filePath, token, callback) {
	var url = path + '?token=' + token + '&action=upload-file&path=' + filePath;
	console.log(url);
	var request = http.get(url, function (response) {
		response.on('data', function (data) {
			var result = JSON.parse(data.toString());
			console.log(result);
			callback(result.magnet);
		});
	})
	request.on('error', function(err) {
		console.log(err);
	});
}

function quit(token) {
	var url = path + '?token=' + token + '&action=quit'
	var request = http.get(url, function (response) {
		response.on('data', function (data) {
			console.log(data.toString());
		})
	});
}

function getProgress(magnet, token) {
	var url = path + '?token=' + token + '&action=getprogress&magnet=' + magnet;
	var request = http.get(url, function (response) {
		response.on('data', function (data) {
			console.log(data.toString());
		});
	});
}

function getFile(magnet, token, key) {
	var url = path + '?token=' + token + '&action=retrieve-file&magnet=' + magnet + '&key=' + key;
	console.log(url);
	var request = http.get(url, function (response) {
		response.on('data', function (data) {
			console.log(data.toString());
		});
	});
}

function getConnected(token) {
	var url = path + '?token=' + token + '&action=isconnected';
	console.log(url);
	var request = http.get(url, function (response) {
		response.on('data', function (data) {
			console.log(data.toString());
		});
	});
}

function test(token) {
	fs.readdir(testdir, function (err, files) {
		var file = files.pop();
		var max = 0;
		for (var i = 0; i < files.length; i++) {
			if (files[i].indexOf('torrent') === -1 && +files[i] > max) {
				max = +files[i];
			}
		}
		console.log(max);
		createTestFile(max + 1, function (path) {
			uploadFile(path, token, function () {});
		});
	});
}

function checkTorrents(token) {
	var url = path + '?token=' + token + '&action=check-torrents';
	console.log(url);
	var request = http.get(url, function (response) {
		response.on('data', function(data) {
			console.log(data.toString());
		});
	});
}

function iterateUpload(token) {
	var min = 100;
	var max = 200;
	cleanDirectory(torrtestdir);
	var files = [];
	function request() {
		if (files.length !== 0) {
			var file = files.pop();
			uploadFile(file, token, request);
		}
	}
	for (var i = min; i < max; i++) {
		createTestFile('t' + i, torrtestdir, function (path) {
			files.push(path);
			if (files.length === max - min) {
				request();
			}
		});
	}
}

function iterateAdding(token) {
	var files = fs.readdirSync(torrtestdir).filter(function (file) {
		return file.indexOf('.torrent') !== -1;
	}).map(function (file) {
		return torrtestdir + '/' + file;
	});
	function request() {
		if (files.length !== 0) {
			var file = files.pop();
			addTorrent(file, token, request);
		}
	}
	request();
}

function iterateUploadAggr(token) {
	var min = 0;
	var max = 100;
	var attrs = [];
	var aggr;

	cleanDirectory(testdir);
	getAggr(function (newAggr) {
		aggr = newAggr;
		main()
	});

	function onRequestResult(magnet) {
		aggr.writeline(magnet, request);
	}
	function request() {
		var r = attrs.pop();
		if (attrs.length > 0) {
			uploadFile(r, token, onRequestResult);
		}
	}

	function main () {
		for (var i = min; i < max; i++) {
			createTestFile(i, testdir, function (path) {
				attrs.push(path);
				if(attrs.length === max - 1) {
					request();
				}
			});
		}
	}
}

function cleanDirectory(dir) {
	console.log('Cleaning directory ', dir);
	fs.readdir(dir, function (err, files) {
		if (!files) {
			console.log('no such directory, creating ', dir);
			fs.mkdirSync(dir);
			return;
		}
		for(var i = 0; i < files.length; i++) {
			console.log('removing file ', files[i]);
			fs.unlinkSync(dir + '/' + files[i]);
		}
	});
}

function getAggr(callback) {
	var path = testdir + '/' + aggrName;
	fs.writeFile(path, '', function () {
		console.log('aggr created');
		callback({
			writeline: function (data, callback) {
				fs.appendFile(path, data + '\n', callback);
			}
		});
	});
}

function createTestFile(name, dir, callback) {
	var path = dir + '/' + name + '.txt';
	var text = 'sample file #' + Math.random();
	for (var i = 0; i < 100000; i++) {
		text += Math.random();
	}
	fs.writeFile(path, text, function(err) {
		if (err) {
			console.log('Creating file error', err);
			return;
		}
		console.log('>> File ' + path + ' created');
		callback(path);
	});
}

function iterateAddingAggr(token) {
	var aggrPath = testdir + '/' + aggrName;
	var content = fs.readFileSync(aggrPath, 'utf8').toString();
	var magnets = content.split('\n');
	magnets = magnets.slice(0, magnets.length - 1);

	function request() {
		if (magnets.length !== 0) {
			var magnet = magnets.pop();
			console.log('ADDING MAGNET: ' + magnet);
			addMagnet(magnet, token, request);
		}
	}
	request();
}

main();
